﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccesoDatos;
using System.Data.SqlClient;
using System.Data;

namespace Juego
{
    public class Jugador
    {

        Acceso acceso = new Acceso();

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private Credencial _credencial = new Credencial();

        public Credencial credencial
        {
            get { return _credencial; }
            set { _credencial = value; }
        }

        private int puntaje;

        public int Puntaje
        {
            get { return puntaje; }
        }

        private int partidasEmpatadas;

        public int PartidasEmpatadas
        {
            get { return partidasEmpatadas; }
        }

        private int partidasPerdidas;

        public int PartidasPerdidas
        {
            get { return partidasPerdidas; }
        }


        private Turno turno = new Turno();
        public Turno Turno
        {
            get { return turno; }
            set { turno = value; }
        }

        private Dado[] misDados = new Dado[5];
        public Dado[] MisDados
        {
            get { return misDados; }
        }

        private int partidasGanadas;

        public int PartidasGanadas
        {
            get { return partidasGanadas; }
        }

        private List<CombinatoriaPuntaje> miscombinatorias = new List<CombinatoriaPuntaje>();

        public List<CombinatoriaPuntaje> MisCombinatorias
        {
            get { return miscombinatorias; }
        }

        private string imagen;

        public string Imagen
        {
            get { return imagen; }
            set { imagen = value; }
        }


        public delegate void delInformar(string mensaje);
        public event delInformar Informar;

        public void tirar()
        {
            int retenidos = 0;
            for (int i = 0; i < misDados.Length; i++)
            {
                if(misDados[i].Retenido)
                {
                    retenidos++;
                }
            }
            if(retenidos < 5)
            {
                if (turno.CantidadTiradas > 0)
                {
                    if (turno.CantidadTiradas == 3)
                    {
                        for (int i = 0; i < misDados.Length; i++)
                        {
                            misDados[i].Reiniciar();
                        }
                    }
                    for (int i = 0; i < misDados.Length; i++)
                    {
                        misDados[i].Girar();
                    }
                    turno.disminuirTiradas();
                }
                else
                {
                    Informar("No puede volver a tirar");
                }
            }
            else
            {
                Informar("Debe soltar un dado antes de tirar");
            }
        }

        public void Retener_Soltar(int numero)
        {
            misDados[numero].Retenido = !misDados[numero].Retenido;
        }

        public void AgregarCombinatorias()
        {
            miscombinatorias.Clear();
            miscombinatorias.Add(new Generala());
            miscombinatorias.Add(new Poker());
            miscombinatorias.Add(new Full());
            miscombinatorias.Add(new Escalera());
            miscombinatorias.Add(new Combinatoria_Uno());
            miscombinatorias.Add(new Combinatoria_dos());
            miscombinatorias.Add(new Combinatoria_tres());
            miscombinatorias.Add(new Combinatoria_cuatro());
            miscombinatorias.Add(new Combinatoria_cinco());
            miscombinatorias.Add(new Combinatoria_seis());
        }
        
        public void ReiniciarPuntaje()
        {
            puntaje = 0;
        }
        public void SumarPuntos()
        {
            puntaje = 0;
            foreach(CombinatoriaPuntaje cp in miscombinatorias)
            {
                puntaje += cp.Puntaje;
            }
        }

        public void FinalizarTurno()
        {
            turno.Reiniciar();
        }

        
        public void Ganar()
        {
            partidasGanadas++;
            Editar();
        }

        public void Perder()
        {
            partidasPerdidas++;
            Editar();
        }

        public void Empatar()
        {
            partidasEmpatadas++;
            Editar();
        }

        public static List<Jugador> Listar()
        {
            List<Jugador> jugadores = new List<Jugador>();
            Acceso acceso = new Acceso();
            DataTable tabla = acceso.Leer("LISTAR_JUGADOR", null);
            foreach(DataRow registro in tabla.Rows)
            {
                Jugador j = new Jugador();
                j.id = int.Parse(registro["id_jugador"].ToString());
                j.nombre = registro["nombre"].ToString();
                j.credencial.Usuario = registro["Usuario"].ToString();
                j.credencial.Contraseña = registro["contraseña"].ToString();
                j.partidasGanadas = int.Parse(registro["PartidasGanadas"].ToString());
                j.partidasEmpatadas = int.Parse(registro["PartidasEmpatadas"].ToString());
                j.partidasPerdidas = int.Parse(registro["PartidasPerdidas"].ToString());
                j.imagen = registro["Imagen"].ToString();
                jugadores.Add(j);
            }
            return jugadores;
        }
        
        public int Insertar()
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@usuario", this.credencial.Usuario));
            if (acceso.Buscar("VERIFICAR_JUGADOR",parametros) == 0)
            {
                parametros.Clear();
                parametros.Add(acceso.CrearParametro("@usuario", this.credencial.Usuario));
                parametros.Add(acceso.CrearParametro("@nombre", this.nombre));
                parametros.Add(acceso.CrearParametro("@contra", this.credencial.Contraseña));
                parametros.Add(acceso.CrearParametro("@imagen", this.imagen));
                int resultado = acceso.Escribir("INSERTAR_JUGADOR", parametros);
                return resultado;
            }
            else
            {
                Informar("Usuario ya registrado");
                return -1;
            }
        }

        public int Editar()
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", this.id));
            parametros.Add(acceso.CrearParametro("@PartidasGanadas", this.partidasGanadas));
            parametros.Add(acceso.CrearParametro("@PartidasEmpatadas", this.partidasEmpatadas));
            parametros.Add(acceso.CrearParametro("@PartidasPerdidas", this.partidasPerdidas));
            int resultado = acceso.Escribir("EDITAR_JUGADOR", parametros);
            return resultado;
        }

        public bool IniciarSesion()
        {
            bool ok = false;
            foreach(Jugador j in Jugador.Listar())
            {
                if(this.credencial.Usuario.Equals(j.credencial.Usuario))
                {
                    if(this.credencial.Contraseña.Equals(j.credencial.Contraseña))
                    {
                        ok = true;
                        this.id = j.id;
                        this.nombre = j.nombre;
                        this.partidasGanadas = j.partidasGanadas;
                        this.partidasEmpatadas = j.partidasEmpatadas;
                        this.partidasPerdidas = j.partidasPerdidas;
                        this.imagen = j.imagen;
                        for(int i = 0; i < misDados.Length; i++)
                        {
                            misDados[i] = new Dado();
                        }
                    }
                }
            }
            if(!ok)
            {
                Informar("Usuario o contraseña incorrectos.");
            }
            return ok;
        }

        public bool VerificarCombinatoriasCompletas()
        {
            bool ok = false;
            int verificador = 0;
            foreach(CombinatoriaPuntaje c in MisCombinatorias)
            {
                if(c.Consumido)
                {
                    verificador++;
                }
            }
            if(verificador == 10)
            {
                ok = true;
            }
            return ok;
        }

        public void ConsumirCombinatoria(CombinatoriaPuntaje cp)
        {
            CombinatoriaPuntaje combinatoria = (from CombinatoriaPuntaje combi in this.MisCombinatorias where combi.Nombre.Equals(cp.Nombre) select combi).FirstOrDefault();
            if(combinatoria != null)
            {
                combinatoria.Puntaje = cp.Puntaje;
            }
            combinatoria.Consumir();
        }

        public override string ToString()
        {
            return this.credencial.Usuario;
        }
    }
}