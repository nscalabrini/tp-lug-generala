﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Juego
{
    public abstract class CombinatoriaPuntaje
    {
        protected string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        protected int puntaje;

        public int Puntaje
        {
            get { return puntaje; }
            set { puntaje = value; }
        }

        protected bool consumido = false;

        public bool Consumido
        {
            get { return consumido; }
        }

        public abstract int Puntuar(Jugador j);

        public void Consumir()
        {
            consumido = true;
        }

        
    }
}