﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AccesoDatos;

namespace Juego
{
    public class Partida
    {
        Acceso acceso = new Acceso();

        public Partida()
        {
            this.AsignarID();
            fecha = DateTime.Now.Ticks;
        }

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private long fecha;

        public DateTime Fecha
        {
            get { return new DateTime(fecha); }
            set { fecha = value.Ticks; }
        }


        private Jugador activo;

        public Jugador Activo
        {
            get { return activo; }
            set { activo = value; }
        }


        private List<Jugador> jugadores = new List<Jugador>();

        public List<Jugador> Jugadores
        {
            get { return jugadores; }
        }

        private Jugador ganador;

        public Jugador Ganador
        {
            get { return ganador; }
        }

        public List<CombinatoriaPuntaje> EvaluarCombinatoria()
        {
            List<CombinatoriaPuntaje> Combinatorias = new List<CombinatoriaPuntaje>();
            
            if (!activo.MisCombinatorias[0].Consumido)
            {
                if(Generala.Evaluar(activo))
                {
                    Generala g = new Generala();
                    g.Puntuar(activo);
                    Combinatorias.Add(g);
                }
                else
                {
                    Generala g = new Generala();
                    Combinatorias.Add(g);
                }
                
            }
            if(!activo.MisCombinatorias[2].Consumido)
            {
                if(Full.Evaluar(activo))
                {
                    Full f = new Full();
                    f.Puntuar(activo);
                    Combinatorias.Add(f);
                }
                else
                {
                    Full f = new Full();
                    Combinatorias.Add(f);
                }
                
            }
            if(!activo.MisCombinatorias[3].Consumido)
            {
                if(Escalera.Evaluar(activo))
                {
                    Escalera e = new Escalera();
                    e.Puntuar(activo);
                    Combinatorias.Add(e);
                }
                else
                {
                    Escalera e = new Escalera();
                    Combinatorias.Add(e);
                }
            }
            if (!activo.MisCombinatorias[1].Consumido)
            {
                if(Poker.Evaluar(activo))
                {
                    Poker p = new Poker();
                    p.Puntuar(activo);
                    Combinatorias.Add(p);
                }
                else
                {
                    Poker p = new Poker();
                    Combinatorias.Add(p);
                }
            }
            if(!activo.MisCombinatorias[4].Consumido)
            {
                Combinatoria_Uno cu = new Combinatoria_Uno();
                cu.Contar(activo);
                cu.Puntuar(activo);
                Combinatorias.Add(cu);
            }
            if (!activo.MisCombinatorias[5].Consumido)
            {
                Combinatoria_dos cd = new Combinatoria_dos();
                cd.Contar(activo);
                cd.Puntuar(activo);
                Combinatorias.Add(cd);
            }
            if (!activo.MisCombinatorias[6].Consumido)
            {
                Combinatoria_tres ct = new Combinatoria_tres();
                ct.Contar(activo);
                ct.Puntuar(activo);
                Combinatorias.Add(ct);
            }
            if (!activo.MisCombinatorias[7].Consumido)
            {
                Combinatoria_cuatro cc = new Combinatoria_cuatro();
                cc.Contar(activo);
                cc.Puntuar(activo);
                Combinatorias.Add(cc);
            }
            if (!activo.MisCombinatorias[8].Consumido)
            {
                Combinatoria_cinco cci = new Combinatoria_cinco();
                cci.Contar(activo);
                cci.Puntuar(activo);
                Combinatorias.Add(cci);
            }
            if(!activo.MisCombinatorias[9].Consumido)
            {
                Combinatoria_seis cs = new Combinatoria_seis();
                cs.Contar(activo);
                cs.Puntuar(activo);
                Combinatorias.Add(cs);
            }
            return Combinatorias;
        }
            
        public void AgregarJugador(Jugador j)
        {
            jugadores.Add(j);
        }

        public void IniciarPartida()
        {
            foreach(Jugador j in jugadores)
            {
                j.ReiniciarPuntaje();
                j.AgregarCombinatorias();
            }
            activo = jugadores[0];
        }

       
        public void DeterminarGanador()
        {
            int puntajeMaximo = jugadores[0].Puntaje;
            foreach (Jugador j in jugadores)
            {
                if(puntajeMaximo < j.Puntaje)
                {
                    puntajeMaximo = j.Puntaje;
                }
            }
            List<Jugador> Ganadores = (from Jugador j in this.Jugadores where j.Puntaje.CompareTo(puntajeMaximo) == 0 select j).ToList();
            if(Ganadores.Count > 1)
            {
                foreach(Jugador j in Ganadores)
                {
                    j.Empatar();
                }
                foreach(Jugador j in jugadores)
                {
                    if(j.Puntaje <  puntajeMaximo)
                    {
                        j.Perder();
                    }
                }
                Finalizar("EMPATE");
            }
            else
            {
                ganador = Ganadores[0];
                ganador.Ganar();
                foreach (Jugador j in jugadores)
                {
                    if (j.Puntaje < ganador.Puntaje)
                    {
                        j.Perder();
                    }
                }
                Finalizar("FINAL");
            }
        }

        public bool CambiarJugadorActivo()
        {
            activo.SumarPuntos();
            activo.FinalizarTurno();
            bool finalPartida = false;
            if (jugadores.IndexOf(activo) + 1 < jugadores.Count)
            {
                activo = jugadores[jugadores.IndexOf(activo) + 1];
            }
            else
            {
                if (!jugadores[0].VerificarCombinatoriasCompletas())
                {
                    activo = jugadores[0];
                }
                else
                {
                    finalPartida = true;
                    FinalizarPartida();
                }
            }
            return finalPartida;
        }

        public void FinalizarPartida()
        {
            DeterminarGanador();
            Insertar();
        }

        public int Insertar()
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", this.id));
            if(this.ganador != null)
            {
                parametros.Add(acceso.CrearParametro("@id_Ganador", ganador.ID));
            }
            else
            {
                parametros.Add(acceso.CrearParametro("@id_Ganador", 0));
            }
            parametros.Add(acceso.CrearParametro("@fecha", this.fecha));
            int resultado = acceso.Escribir("INSERTAR_PARTIDA", parametros);
            parametros.Clear();
            foreach (Jugador j in jugadores)
            {
                parametros.Clear();
                parametros.Add(acceso.CrearParametro("@id", this.id));
                parametros.Add(acceso.CrearParametro("@id_jugador", j.ID));
                parametros.Add(acceso.CrearParametro("@puntaje", j.Puntaje));
                resultado = acceso.Escribir("INSERTAR_PARTIDA_JUGADOR", parametros);
            }
            return resultado;
        }

        public void AsignarID()
        {

            int numero = acceso.Validar("ASIGNAR_ID_PARTIDA", null);
            if(numero != 0)
            {
                this.id = numero;
            }
            else
            {
                Informar("Ocurrio un error");
            }
            
        }

        public delegate void delInformar(string mensaje);
        public event delInformar Informar;
        public delegate void delFinalizar(string mensaje);
        public event delFinalizar Finalizar;
    }
}