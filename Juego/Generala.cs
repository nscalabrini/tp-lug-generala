﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Juego
{
    public class Generala : CombinatoriaPuntaje
    {
        public Generala()
        {
            nombre = "Generala";
        }

        public static bool Evaluar(Jugador j)
        {
            bool ok = false;
            if (j.MisDados[0].Valor == j.MisDados[1].Valor && j.MisDados[0].Valor == j.MisDados[2].Valor && j.MisDados[0].Valor == j.MisDados[3].Valor && j.MisDados[0].Valor == j.MisDados[4].Valor)
            {
                ok = true;
            }
            return ok;
        }

        public override int Puntuar(Jugador j)
        {
                if(j.Turno.CantidadTiradas == 2)
                {
                    puntaje = 60;
                    return puntaje;
                }
                else
                {
                    puntaje = 50;
                    return puntaje;
                }
        }

        public override string ToString()
        {
            return nombre;
        }
    }
}