﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Juego
{
    public class Combinatoria_cinco : CombinatoriaPuntaje
    {
        public Combinatoria_cinco()
        {
            nombre = "Cinco";
        }

        int cuantos = 0;

        public void Contar(Jugador j)
        {
            for (int i = 0; i < j.MisDados.Length; i++)
            {
                if (j.MisDados[i].Valor == 5)
                {
                    cuantos++;
                }
            }
        }

        public override int Puntuar(Jugador j)
        {
            puntaje = cuantos * 5;
            return puntaje;
        }

        public override string ToString()
        {
            return nombre;
        }
    }
}