﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Juego
{
    public class Combinatoria_seis : CombinatoriaPuntaje
    {
        public Combinatoria_seis()
        {
            nombre = "Seis";
        }

        int cuantos = 0;

        public void Contar(Jugador j)
        {
            for (int i = 0; i < j.MisDados.Length; i++)
            {
                if (j.MisDados[i].Valor == 6)
                {
                    cuantos++;
                }
            }
        }

        public override int Puntuar(Jugador j)
        {
            puntaje = cuantos * 6;
            return puntaje;
        }

        public override string ToString()
        {
            return nombre;
        }
    }
}