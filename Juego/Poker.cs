﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Juego
{
    public class Poker : CombinatoriaPuntaje
    {
        public Poker()
        {
            nombre = "Poker";
        }

        public static bool Evaluar(Jugador j)
        {
            int[] numeros = new int[7];
            bool ok = false;
            for(int i = 0; i < j.MisDados.Length; i++)
            {
                if(j.MisDados[i].Valor == 1)
                {
                    numeros[1]++;
                }
                if (j.MisDados[i].Valor == 2)
                {
                    numeros[2]++;
                }
                if (j.MisDados[i].Valor == 3)
                {
                    numeros[3]++;
                }
                if (j.MisDados[i].Valor == 4)
                {
                    numeros[4]++;
                }
                if (j.MisDados[i].Valor == 5)
                {
                    numeros[5]++;
                }
                if (j.MisDados[i].Valor == 6)
                {
                    numeros[6]++;
                }
            }
            for (int i = 0; i < numeros.Length; i++)
            {
                if (numeros[i] == 4)
                {
                    ok = true;
                }
            }
            return ok;
        }
        public override int Puntuar(Jugador j)
        {
            if(j.Turno.CantidadTiradas == 2)
            {
                puntaje = 45;
                return puntaje;
            }
            else
            {
                puntaje = 40;
                return puntaje;
            }
        }

        public override string ToString()
        {
            return nombre;
        }
    }
}