﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Juego
{
    public class Turno
    {
        public static int numero = 0;

        private int cantidadTiradas = 3;

        public int CantidadTiradas
        {
            get { return cantidadTiradas; }
        }
        
        public void disminuirTiradas()
        {
            if(cantidadTiradas > 0)
            {
                cantidadTiradas--;
            }
        }

       public void Reiniciar()
        {
            cantidadTiradas = 3;
            numero++;
        }
    }
}