﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Juego
{
    public class Dado
    {
        static Random rnd ;
        static Dado()
        {
            
            DateTime Tiempo = DateTime.Now;
            Tiempo.AddTicks(DateTime.Now.Ticks);
            int semilla = (int)Tiempo.Millisecond;
            rnd = new Random(semilla);
        }

        private int valor;

        public int Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        private bool retenido = false;

        public bool Retenido
        {
            get { return retenido; }
            set { retenido = value; }
        }
        
        public void Reiniciar()
        {
            retenido = false;
        }
        public void Girar()
        {
            if(!retenido)
            {
                valor = rnd.Next(1, 7);
            }
            
        }
    }
}
