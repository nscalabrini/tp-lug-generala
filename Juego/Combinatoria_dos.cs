﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Juego
{
    public class Combinatoria_dos : CombinatoriaPuntaje
    {
        public Combinatoria_dos()
        {
            nombre = "Dos";
        }

        int cuantos = 0;

        public void Contar(Jugador j)
        {
            for (int i = 0; i < j.MisDados.Length; i++)
            {
                if (j.MisDados[i].Valor == 2)
                {
                    cuantos++;
                }
            }
        }

        public override int Puntuar(Jugador j)
        {
            puntaje = cuantos * 2;
            return puntaje;
        }

        public override string ToString()
        {
            return nombre;
        }
    }
}