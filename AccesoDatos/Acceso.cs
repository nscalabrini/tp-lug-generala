﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos
{
    public class Acceso
    {
        SqlConnection conexion;

        public void Abrir()
        {
            conexion = new SqlConnection();
            conexion.ConnectionString = @"Data Source= .\; Initial Catalog= Generala; Integrated Security =  SSPI";
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion.Dispose();
            conexion = null;
            GC.Collect();
        }

        public SqlCommand CrearComando(string nombre, List<SqlParameter> parametros)
        {
            SqlCommand comando = new SqlCommand(nombre, conexion);
            comando.CommandType = CommandType.StoredProcedure;
            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }
            return comando;
        }

        public int Escribir(string nombre, List<SqlParameter> parametros)
        {
            Abrir();
            int filas = 0;
            SqlCommand comando = CrearComando(nombre, parametros);
            try
            {
                filas = comando.ExecuteNonQuery();
            }
            catch
            {
                filas = -1;
            }
            comando = null;
            Cerrar();
            return filas;
        }

        public int Buscar(string nombre, List<SqlParameter> parametros)
        {
            Abrir();
            int filas = 0;
            SqlCommand comando = CrearComando(nombre, parametros);
            try
            {
                filas = int.Parse(comando.ExecuteScalar().ToString());
            }
            catch
            {
                filas = -1;
            }
            comando = null;
            Cerrar();
            return filas;
        }
      

        public DataTable Leer (string nombre, List<SqlParameter> parametros)
        {
            Abrir();
            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = CrearComando(nombre, parametros);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            adaptador = null;
            Cerrar();
            return tabla;
        }

        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.Int32;
            return parametro;
        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.String;
            return parametro;
        }

        public SqlParameter CrearParametro(string nombre, long valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.Decimal;
            return parametro;
        }

        public int Validar(string nombre, List<SqlParameter> parametros)
        {
            Abrir();
            int num = 0;
            SqlCommand cmd = CrearComando(nombre, parametros);
            try
            {
                num = int.Parse(cmd.ExecuteScalar().ToString()) + 1;
            }
            catch
            {
                
            }
            return num;
        }
    }
}
