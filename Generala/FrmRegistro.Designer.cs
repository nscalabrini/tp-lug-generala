﻿namespace Generala
{
    partial class FrmRegistro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.miCasilla3 = new Generala.MiCasilla();
            this.miCasilla2 = new Generala.MiCasilla();
            this.miCasilla1 = new Generala.MiCasilla();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.button1.Location = new System.Drawing.Point(564, 385);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 35);
            this.button1.TabIndex = 3;
            this.button1.Text = "Crear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.button2.Location = new System.Drawing.Point(100, 385);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 35);
            this.button2.TabIndex = 4;
            this.button2.Text = "Cancelar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Homero",
            "Bart",
            "Lisa",
            "Marge",
            "Maggie",
            "Abuelo",
            "Burns"});
            this.comboBox1.Location = new System.Drawing.Point(190, 274);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(403, 28);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(116, 278);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Imagen:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(700, 37);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(344, 342);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // miCasilla3
            // 
            this.miCasilla3.Label = "Clave:";
            this.miCasilla3.Location = new System.Drawing.Point(100, 172);
            this.miCasilla3.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.miCasilla3.Name = "miCasilla3";
            this.miCasilla3.Requerido = true;
            this.miCasilla3.Size = new System.Drawing.Size(576, 68);
            this.miCasilla3.TabIndex = 7;
            this.miCasilla3.Texto = "";
            this.miCasilla3.Load += new System.EventHandler(this.miCasilla3_Load);
            // 
            // miCasilla2
            // 
            this.miCasilla2.Label = "Usuario:";
            this.miCasilla2.Location = new System.Drawing.Point(100, 95);
            this.miCasilla2.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.miCasilla2.Name = "miCasilla2";
            this.miCasilla2.Requerido = true;
            this.miCasilla2.Size = new System.Drawing.Size(576, 68);
            this.miCasilla2.TabIndex = 6;
            this.miCasilla2.Texto = "";
            // 
            // miCasilla1
            // 
            this.miCasilla1.Label = "Nombre:";
            this.miCasilla1.Location = new System.Drawing.Point(100, 18);
            this.miCasilla1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.miCasilla1.Name = "miCasilla1";
            this.miCasilla1.Requerido = true;
            this.miCasilla1.Size = new System.Drawing.Size(576, 68);
            this.miCasilla1.TabIndex = 5;
            this.miCasilla1.Texto = "";
            this.miCasilla1.Load += new System.EventHandler(this.miCasilla1_Load);
            // 
            // FrmRegistro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(1088, 472);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.miCasilla3);
            this.Controls.Add(this.miCasilla2);
            this.Controls.Add(this.miCasilla1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmRegistro";
            this.Text = "FrmRegistro";
            this.Load += new System.EventHandler(this.FrmRegistro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private MiCasilla miCasilla1;
        private MiCasilla miCasilla2;
        private MiCasilla miCasilla3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}