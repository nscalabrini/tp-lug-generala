﻿namespace Generala
{
    partial class Principal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.cuadroLogin4 = new Generala.CuadroLogin();
            this.cuadroLogin3 = new Generala.CuadroLogin();
            this.cuadroLogin2 = new Generala.CuadroLogin();
            this.cuadroLogin1 = new Generala.CuadroLogin();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(902, 346);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(228, 89);
            this.button1.TabIndex = 0;
            this.button1.Text = "Jugar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(902, 471);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(228, 86);
            this.button2.TabIndex = 1;
            this.button2.Text = "Registrarse";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Font = new System.Drawing.Font("Algerian", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(843, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(417, 80);
            this.label1.TabIndex = 6;
            this.label1.Text = "Generala";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(902, 585);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(228, 86);
            this.button3.TabIndex = 7;
            this.button3.Text = "Estadisticas";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(902, 702);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(228, 86);
            this.button4.TabIndex = 8;
            this.button4.Text = "Bitacora";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // cuadroLogin4
            // 
            this.cuadroLogin4.BackColor = System.Drawing.SystemColors.HotTrack;
            this.cuadroLogin4.Location = new System.Drawing.Point(0, 665);
            this.cuadroLogin4.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cuadroLogin4.Name = "cuadroLogin4";
            this.cuadroLogin4.Size = new System.Drawing.Size(732, 231);
            this.cuadroLogin4.TabIndex = 5;
            this.cuadroLogin4.Texto1 = "";
            this.cuadroLogin4.Texto2 = "";
            // 
            // cuadroLogin3
            // 
            this.cuadroLogin3.BackColor = System.Drawing.SystemColors.HotTrack;
            this.cuadroLogin3.Location = new System.Drawing.Point(0, 445);
            this.cuadroLogin3.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cuadroLogin3.Name = "cuadroLogin3";
            this.cuadroLogin3.Size = new System.Drawing.Size(732, 231);
            this.cuadroLogin3.TabIndex = 4;
            this.cuadroLogin3.Texto1 = "";
            this.cuadroLogin3.Texto2 = "";
            // 
            // cuadroLogin2
            // 
            this.cuadroLogin2.BackColor = System.Drawing.SystemColors.HotTrack;
            this.cuadroLogin2.Location = new System.Drawing.Point(0, 220);
            this.cuadroLogin2.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cuadroLogin2.Name = "cuadroLogin2";
            this.cuadroLogin2.Size = new System.Drawing.Size(732, 231);
            this.cuadroLogin2.TabIndex = 3;
            this.cuadroLogin2.Texto1 = "";
            this.cuadroLogin2.Texto2 = "";
            // 
            // cuadroLogin1
            // 
            this.cuadroLogin1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.cuadroLogin1.Location = new System.Drawing.Point(0, 2);
            this.cuadroLogin1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cuadroLogin1.Name = "cuadroLogin1";
            this.cuadroLogin1.Size = new System.Drawing.Size(732, 231);
            this.cuadroLogin1.TabIndex = 2;
            this.cuadroLogin1.Texto1 = "";
            this.cuadroLogin1.Texto2 = "";
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(1312, 895);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cuadroLogin4);
            this.Controls.Add(this.cuadroLogin3);
            this.Controls.Add(this.cuadroLogin2);
            this.Controls.Add(this.cuadroLogin1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Principal";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Principal_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private CuadroLogin cuadroLogin1;
        private CuadroLogin cuadroLogin2;
        private CuadroLogin cuadroLogin3;
        private CuadroLogin cuadroLogin4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}

