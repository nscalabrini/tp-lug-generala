﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Juego;

namespace Generala
{
    public partial class CU_UsuarioActivocs : UserControl
    {
        public CU_UsuarioActivocs()
        {
            InitializeComponent();
        }

        private void CU_UsuarioActivocs_Load(object sender, EventArgs e)
        {

        }

        public string Texto
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }

        public void CambiarLabel(Jugador j)
        {
            label1.Text = j.credencial.Usuario;
        }
       
        public void CambiarColor(int num)
        {
            if(num == 1)
            {
                this.BackColor = Color.Blue;
                label1.ForeColor = Color.White;
            }
            else if( num == 0)
            {
                this.BackColor = Color.Green;
            }
        }

        public void CambiarImagen(Jugador j)
        {
            pictureBox1.Image = Image.FromFile(j.Imagen);
        }
    }
}
