﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Generala
{
    public partial class FrmBitacora : Form
    {
        public FrmBitacora()
        {
            InitializeComponent();
        }

        Bitacora bitacora = new Bitacora();
        XmlNode nodo;
        private void button1_Click(object sender, EventArgs e)
        {
            IniciarBotones();
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = bitacora.Leer().Tables[0];
            nodo = bitacora.LoadLogin();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                nodo = bitacora.MostrarSiguiente(nodo);
                Mostrar();
            }
            catch
            {
                MessageBox.Show("No se puede realizar");
            }
            
        }

        void IniciarBotones()
        {
            button6.Enabled = true;
            button7.Enabled = true;
            button4.Enabled = false;
            button5.Enabled = false;
            button8.Enabled = false;
        }

        void Mostrar()
        {
            if(nodo != null)
            {
                label1.Text = nodo.Value;
                label2.Text = nodo.InnerText;
                label3.Text = nodo.InnerXml;
                label4.Text = nodo.Name;
            }
            else
            {
                MessageBox.Show("Debe volver a cargar");
                label1.Visible = false;
                label2.Visible = false;
                label3.Visible = false;
                label4.Visible = false;
            }
        }

        private void FrmBitacora_Load(object sender, EventArgs e)
        {
            label1.ForeColor = Color.White;
            label2.ForeColor = Color.White;
            label3.ForeColor = Color.White;
            label4.ForeColor = Color.White;
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            button4.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = false;
            button8.Enabled = false;


        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                nodo = bitacora.MostrarAnterior(nodo);
                label1.Visible = true;
                label2.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
                Mostrar();
            }
            catch
            {
                MessageBox.Show("No se puede realizar");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            button4.Enabled = true;
            button5.Enabled = true;
            button8.Enabled = true;
            nodo = bitacora.MostrarPrimero(nodo);
            label1.Visible = true;
            label2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            Mostrar();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                button4.Enabled = true;
                button5.Enabled = true;
                button8.Enabled = true;
                nodo = bitacora.MostrarUltimo(nodo);
                label1.Visible = true;
                label2.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
                Mostrar();
            }
            catch
            {
                MessageBox.Show("No se puede realizar");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            button6.Enabled = true;
            button7.Enabled = true;
            nodo = bitacora.MostrarPadre(nodo);
            Mostrar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            IniciarBotones();
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = bitacora.LeerMovimientos().Tables[0];
            nodo = bitacora.LoadMovimiento();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            IniciarBotones();
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = bitacora.LeerPartida().Tables[0];
            nodo = bitacora.LoadPartida();
        }
    }
}
