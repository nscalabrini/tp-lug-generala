﻿namespace Generala
{
    partial class FrmPartida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.LBLturnosRestamtes = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.LBLturno = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cU_UsuarioActivocs4 = new Generala.CU_UsuarioActivocs();
            this.cU_UsuarioActivocs3 = new Generala.CU_UsuarioActivocs();
            this.cU_UsuarioActivocs2 = new Generala.CU_UsuarioActivocs();
            this.cU_UsuarioActivocs1 = new Generala.CU_UsuarioActivocs();
            this.cU_DADO5 = new Generala.CU_DADO();
            this.cU_DADO4 = new Generala.CU_DADO();
            this.cU_DADO3 = new Generala.CU_DADO();
            this.cU_DADO2 = new Generala.CU_DADO();
            this.cU_DADO1 = new Generala.CU_DADO();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(360, 235);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(189, 29);
            this.label5.TabIndex = 8;
            this.label5.Text = "Combinatorias:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(366, 271);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.Size = new System.Drawing.Size(360, 395);
            this.dataGridView1.TabIndex = 9;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(1176, 289);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 62;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(360, 386);
            this.dataGridView2.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(938, 308);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(180, 71);
            this.button1.TabIndex = 11;
            this.button1.Text = "Tirar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(938, 388);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(180, 97);
            this.button2.TabIndex = 12;
            this.button2.Text = "Finalizar Turno";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // LBLturnosRestamtes
            // 
            this.LBLturnosRestamtes.AutoSize = true;
            this.LBLturnosRestamtes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBLturnosRestamtes.Location = new System.Drawing.Point(849, 522);
            this.LBLturnosRestamtes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLturnosRestamtes.Name = "LBLturnosRestamtes";
            this.LBLturnosRestamtes.Size = new System.Drawing.Size(230, 29);
            this.LBLturnosRestamtes.TabIndex = 13;
            this.LBLturnosRestamtes.Text = "Tiradas restantes: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1170, 254);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(173, 29);
            this.label7.TabIndex = 14;
            this.label7.Text = "Posibilidades";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(748, 700);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(426, 35);
            this.progressBar1.TabIndex = 24;
            // 
            // LBLturno
            // 
            this.LBLturno.AutoSize = true;
            this.LBLturno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBLturno.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.LBLturno.Location = new System.Drawing.Point(885, 665);
            this.LBLturno.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLturno.Name = "LBLturno";
            this.LBLturno.Size = new System.Drawing.Size(111, 29);
            this.LBLturno.TabIndex = 25;
            this.LBLturno.Text = "Turno 1/";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(736, 271);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 29);
            this.label1.TabIndex = 26;
            this.label1.Text = "Puntaje: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Algerian", 27F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(33, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(316, 60);
            this.label2.TabIndex = 27;
            this.label2.Text = "Generala";
            // 
            // cU_UsuarioActivocs4
            // 
            this.cU_UsuarioActivocs4.Location = new System.Drawing.Point(18, 525);
            this.cU_UsuarioActivocs4.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cU_UsuarioActivocs4.Name = "cU_UsuarioActivocs4";
            this.cU_UsuarioActivocs4.Size = new System.Drawing.Size(314, 138);
            this.cU_UsuarioActivocs4.TabIndex = 23;
            this.cU_UsuarioActivocs4.Texto = "label1";
            // 
            // cU_UsuarioActivocs3
            // 
            this.cU_UsuarioActivocs3.Location = new System.Drawing.Point(18, 398);
            this.cU_UsuarioActivocs3.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cU_UsuarioActivocs3.Name = "cU_UsuarioActivocs3";
            this.cU_UsuarioActivocs3.Size = new System.Drawing.Size(314, 138);
            this.cU_UsuarioActivocs3.TabIndex = 22;
            this.cU_UsuarioActivocs3.Texto = "label1";
            // 
            // cU_UsuarioActivocs2
            // 
            this.cU_UsuarioActivocs2.Location = new System.Drawing.Point(18, 274);
            this.cU_UsuarioActivocs2.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cU_UsuarioActivocs2.Name = "cU_UsuarioActivocs2";
            this.cU_UsuarioActivocs2.Size = new System.Drawing.Size(314, 138);
            this.cU_UsuarioActivocs2.TabIndex = 21;
            this.cU_UsuarioActivocs2.Texto = "label1";
            // 
            // cU_UsuarioActivocs1
            // 
            this.cU_UsuarioActivocs1.Location = new System.Drawing.Point(18, 146);
            this.cU_UsuarioActivocs1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cU_UsuarioActivocs1.Name = "cU_UsuarioActivocs1";
            this.cU_UsuarioActivocs1.Size = new System.Drawing.Size(314, 138);
            this.cU_UsuarioActivocs1.TabIndex = 20;
            this.cU_UsuarioActivocs1.Texto = "label1";
            // 
            // cU_DADO5
            // 
            this.cU_DADO5.BackColor = System.Drawing.Color.White;
            this.cU_DADO5.dado = null;
            this.cU_DADO5.Location = new System.Drawing.Point(1340, 40);
            this.cU_DADO5.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cU_DADO5.Name = "cU_DADO5";
            this.cU_DADO5.Size = new System.Drawing.Size(150, 154);
            this.cU_DADO5.TabIndex = 19;
            this.cU_DADO5.Click += new System.EventHandler(this.cU_DADO5_Click);
            // 
            // cU_DADO4
            // 
            this.cU_DADO4.BackColor = System.Drawing.Color.White;
            this.cU_DADO4.dado = null;
            this.cU_DADO4.Location = new System.Drawing.Point(1143, 40);
            this.cU_DADO4.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cU_DADO4.Name = "cU_DADO4";
            this.cU_DADO4.Size = new System.Drawing.Size(150, 154);
            this.cU_DADO4.TabIndex = 18;
            this.cU_DADO4.Click += new System.EventHandler(this.cU_DADO4_Click);
            // 
            // cU_DADO3
            // 
            this.cU_DADO3.BackColor = System.Drawing.Color.White;
            this.cU_DADO3.dado = null;
            this.cU_DADO3.Location = new System.Drawing.Point(938, 40);
            this.cU_DADO3.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cU_DADO3.Name = "cU_DADO3";
            this.cU_DADO3.Size = new System.Drawing.Size(150, 154);
            this.cU_DADO3.TabIndex = 17;
            this.cU_DADO3.Click += new System.EventHandler(this.cU_DADO3_Click);
            // 
            // cU_DADO2
            // 
            this.cU_DADO2.BackColor = System.Drawing.Color.White;
            this.cU_DADO2.dado = null;
            this.cU_DADO2.Location = new System.Drawing.Point(723, 40);
            this.cU_DADO2.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cU_DADO2.Name = "cU_DADO2";
            this.cU_DADO2.Size = new System.Drawing.Size(150, 154);
            this.cU_DADO2.TabIndex = 16;
            this.cU_DADO2.Click += new System.EventHandler(this.cU_DADO2_Click);
            // 
            // cU_DADO1
            // 
            this.cU_DADO1.BackColor = System.Drawing.Color.White;
            this.cU_DADO1.dado = null;
            this.cU_DADO1.Location = new System.Drawing.Point(504, 40);
            this.cU_DADO1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.cU_DADO1.Name = "cU_DADO1";
            this.cU_DADO1.Size = new System.Drawing.Size(150, 154);
            this.cU_DADO1.TabIndex = 15;
            this.cU_DADO1.Click += new System.EventHandler(this.cU_DADO1_Click);
            // 
            // FrmPartida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(1578, 782);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LBLturno);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.cU_UsuarioActivocs4);
            this.Controls.Add(this.cU_UsuarioActivocs3);
            this.Controls.Add(this.cU_UsuarioActivocs2);
            this.Controls.Add(this.cU_UsuarioActivocs1);
            this.Controls.Add(this.cU_DADO5);
            this.Controls.Add(this.cU_DADO4);
            this.Controls.Add(this.cU_DADO3);
            this.Controls.Add(this.cU_DADO2);
            this.Controls.Add(this.cU_DADO1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.LBLturnosRestamtes);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label5);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmPartida";
            this.Text = "FrmPartida";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPartida_FormClosed);
            this.Load += new System.EventHandler(this.FrmPartida_Load);
            this.Shown += new System.EventHandler(this.FrmPartida_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label LBLturnosRestamtes;
        private System.Windows.Forms.Label label7;
        private CU_DADO cU_DADO1;
        private CU_DADO cU_DADO2;
        private CU_DADO cU_DADO3;
        private CU_DADO cU_DADO4;
        private CU_DADO cU_DADO5;
        private CU_UsuarioActivocs cU_UsuarioActivocs1;
        private CU_UsuarioActivocs cU_UsuarioActivocs2;
        private CU_UsuarioActivocs cU_UsuarioActivocs3;
        private CU_UsuarioActivocs cU_UsuarioActivocs4;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label LBLturno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}