﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Juego;



namespace Generala
{
    public partial class FrmEstadisticas : Form
    {
        public FrmEstadisticas()
        {
            InitializeComponent();
        }

        Graphics g;

        private void FrmEstadisticas_Load(object sender, EventArgs e)
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = Jugador.Listar();
            label7.Hide();
            label8.Hide();
            label9.Hide();
            label10.Hide();
        }

        void DibujarTorta(Jugador j)
        {
            float total = j.PartidasGanadas + j.PartidasPerdidas + j.PartidasEmpatadas;
            float num1 = (j.PartidasGanadas/total) * 360F;
            float num2 = (j.PartidasEmpatadas/total) * 360F;
            float num3 = (j.PartidasPerdidas/total) * 360F;
            g = this.CreateGraphics();
            Pen p = new Pen(Color.Black, 2);
            Rectangle rec = new Rectangle(190,200,180,180);
            Brush b1 = new SolidBrush(Color.Red);
            Brush b2 = new SolidBrush(Color.Blue);
            Brush b3 = new SolidBrush(Color.Yellow);
            g.Clear(this.BackColor);
            g.DrawPie(p, rec, 0, num1);
            g.FillPie(b1, rec, 0, num1);
            g.DrawPie(p, rec, num1, num2);
            g.FillPie(b2, rec, num1, num2);
            g.DrawPie(p, rec, num2 + num1, num3);
            g.FillPie(b3, rec, num2 + num1, num3);
            Rectangle rec2 = new Rectangle(40, 220, 20, 20);
            g.DrawRectangle(p, rec2);
            g.FillRectangle(b1, rec2);
            Rectangle rec3 = new Rectangle(40, 270, 20, 20);
            g.DrawRectangle(p, rec3);
            g.FillRectangle(b2, rec3);
            Rectangle rec4 = new Rectangle(40, 320, 20, 20);
            g.DrawRectangle(p, rec4);
            g.FillRectangle(b3, rec4);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Jugador j = (Jugador)comboBox1.SelectedItem;
            label2.Text = "Nombre: " + j.Nombre;
            label3.Text = "Usuario: " + j.credencial.Usuario;
            label4.Text = "Victorias: " + j.PartidasGanadas;
            label5.Text = "Empates: " + j.PartidasEmpatadas;
            label6.Text = "Derrotas: " + j.PartidasPerdidas;
            pictureBox1.Image = Image.FromFile(j.Imagen);
            float promedio = (float.Parse(j.PartidasGanadas.ToString())/float.Parse((j.PartidasGanadas + j.PartidasEmpatadas + j.PartidasPerdidas).ToString())) * 100F;
            label10.Text = "Promedio de victorias: %" + promedio; 
            label7.Show();
            label8.Show();
            label9.Show();
            label10.Show();
            DibujarTorta(j);
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
