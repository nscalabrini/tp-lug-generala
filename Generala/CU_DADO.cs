﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Juego;

namespace Generala
{
    public partial class CU_DADO : UserControl
    {
        public CU_DADO()
        {
            InitializeComponent();
        }

        Graphics graficador;

        private Dado _dado;
        public Dado dado
        {
            get { return _dado; }
            set { _dado = value; }
        }

       
        private void CU_DADO_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.White;
            graficador = this.CreateGraphics();
        }

        public void EscribirPunto()
        {
            EscribirUno();
        }

        public void MostrarValorDado()
        {
            if(!dado.Retenido)
            {
                if (dado.Valor == 1)
                {
                    graficador.Clear(Color.White);
                    EscribirUno();
                }
                else if (dado.Valor == 2)
                {
                    graficador.Clear(Color.White);
                    EscribirDos();
                }
                else if (dado.Valor == 3)
                {
                    graficador.Clear(Color.White);
                    EscribirTres();
                }
                else if (dado.Valor == 4)
                {
                    graficador.Clear(Color.White);
                    EscribirCuatro();
                }
                else if (dado.Valor == 5)
                {
                    graficador.Clear(Color.White);
                    EscribirCinco();
                }
                else
                {
                    graficador.Clear(Color.White);
                    EscribirSeis();
                }
            }
            else
            {
                Retener_Soltar();
            }
        }

        private void EscribirSeis()
        {
            Pen Lapiz = new Pen(Brushes.Black);
            Rectangle rec2 = new Rectangle();
            rec2.Size = new Size(99, 99);
            rec2.Location = new Point(0, 0);
            graficador.DrawRectangle(Lapiz, rec2);
            Rectangle rec = new Rectangle();
            rec.Size = new Size(10, 10);
            rec.Location = new Point(15, 15);
            graficador.FillEllipse(Brushes.Black, rec);
            graficador.DrawEllipse(Lapiz, rec);
            Rectangle rec3 = new Rectangle();
            rec3.Size = new Size(10, 10);
            rec3.Location = new Point(75, 75);
            graficador.FillEllipse(Brushes.Black, rec3);
            graficador.DrawEllipse(Lapiz, rec3);
            Rectangle rec4 = new Rectangle();
            rec4.Size = new Size(10, 10);
            rec4.Location = new Point(75, 15);
            graficador.FillEllipse(Brushes.Black, rec4);
            graficador.DrawEllipse(Lapiz, rec4);
            Rectangle rec5 = new Rectangle();
            rec5.Size = new Size(10, 10);
            rec5.Location = new Point(15, 75);
            graficador.FillEllipse(Brushes.Black, rec5);
            graficador.DrawEllipse(Lapiz, rec5);
            Rectangle rec6 = new Rectangle();
            rec6.Size = new Size(10, 10);
            rec6.Location = new Point(15, 45);
            graficador.FillEllipse(Brushes.Black, rec6);
            graficador.DrawEllipse(Lapiz, rec6);
            Rectangle rec7 = new Rectangle();
            rec7.Size = new Size(10, 10);
            rec7.Location = new Point(75, 45);
            graficador.FillEllipse(Brushes.Black, rec7);
            graficador.DrawEllipse(Lapiz, rec7);
        }

        private void EscribirCinco()
        {
            Pen Lapiz = new Pen(Brushes.Black);
            Rectangle rec2 = new Rectangle();
            rec2.Size = new Size(99, 99);
            rec2.Location = new Point(0, 0);
            graficador.DrawRectangle(Lapiz, rec2);
            Rectangle rec = new Rectangle();
            rec.Size = new Size(10, 10);
            rec.Location = new Point(15, 15);
            graficador.FillEllipse(Brushes.Black, rec);
            graficador.DrawEllipse(Lapiz, rec);
            Rectangle rec3 = new Rectangle();
            rec3.Size = new Size(10, 10);
            rec3.Location = new Point(75, 75);
            graficador.FillEllipse(Brushes.Black, rec3);
            graficador.DrawEllipse(Lapiz, rec3);
            Rectangle rec4 = new Rectangle();
            rec4.Size = new Size(10, 10);
            rec4.Location = new Point(75, 15);
            graficador.FillEllipse(Brushes.Black, rec4);
            graficador.DrawEllipse(Lapiz, rec4);
            Rectangle rec5 = new Rectangle();
            rec5.Size = new Size(10, 10);
            rec5.Location = new Point(15, 75);
            graficador.FillEllipse(Brushes.Black, rec5);
            graficador.DrawEllipse(Lapiz, rec5);
            Rectangle rec6 = new Rectangle();
            rec6.Size = new Size(10, 10);
            rec6.Location = new Point(45, 45);
            graficador.FillEllipse(Brushes.Black, rec6);
            graficador.DrawEllipse(Lapiz, rec6);
        }

        void EscribirUno()
        {
            Rectangle rec = new Rectangle();
            rec.Size = new Size(10, 10);
            rec.Location = new Point(45, 45);
            Pen Lapiz = new Pen(Brushes.Black);
            graficador.FillEllipse(Brushes.Black, rec);
            graficador.DrawEllipse(Lapiz, rec);
            Rectangle rec2 = new Rectangle();
            rec2.Size = new Size(99, 99);
            rec2.Location = new Point(0, 0);
            graficador.DrawRectangle(Lapiz, rec2);
        }

        void EscribirDos()
        {
            Pen Lapiz = new Pen(Brushes.Black);
            Rectangle rec2 = new Rectangle();
            rec2.Size = new Size(99, 99);
            rec2.Location = new Point(0, 0);
            graficador.DrawRectangle(Lapiz, rec2);
            Rectangle rec = new Rectangle();
            rec.Size = new Size(10, 10);
            rec.Location = new Point(15, 15);
            graficador.FillEllipse(Brushes.Black, rec);
            graficador.DrawEllipse(Lapiz, rec);
            Rectangle rec3 = new Rectangle();
            rec3.Size = new Size(10, 10);
            rec3.Location = new Point(75, 75);
            graficador.FillEllipse(Brushes.Black, rec3);
            graficador.DrawEllipse(Lapiz, rec3);
        }

        void EscribirTres()
        {
            Pen Lapiz = new Pen(Brushes.Black);
            Rectangle rec2 = new Rectangle();
            rec2.Size = new Size(99, 99);
            rec2.Location = new Point(0, 0);
            graficador.DrawRectangle(Lapiz, rec2);
            Rectangle rec = new Rectangle();
            rec.Size = new Size(10, 10);
            rec.Location = new Point(15, 15);
            graficador.FillEllipse(Brushes.Black, rec);
            graficador.DrawEllipse(Lapiz, rec);
            Rectangle rec3 = new Rectangle();
            rec3.Size = new Size(10, 10);
            rec3.Location = new Point(75, 75);
            graficador.FillEllipse(Brushes.Black, rec3);
            graficador.DrawEllipse(Lapiz, rec3);
            Rectangle rec4 = new Rectangle();
            rec4.Size = new Size(10, 10);
            rec4.Location = new Point(45, 45);
            graficador.FillEllipse(Brushes.Black, rec4);
            graficador.DrawEllipse(Lapiz, rec4);
        }

        void EscribirCuatro()
        {
            Pen Lapiz = new Pen(Brushes.Black);
            Rectangle rec2 = new Rectangle();
            rec2.Size = new Size(99, 99);
            rec2.Location = new Point(0, 0);
            graficador.DrawRectangle(Lapiz, rec2);
            Rectangle rec = new Rectangle();
            rec.Size = new Size(10, 10);
            rec.Location = new Point(15, 15);
            graficador.FillEllipse(Brushes.Black, rec);
            graficador.DrawEllipse(Lapiz, rec);
            Rectangle rec3 = new Rectangle();
            rec3.Size = new Size(10, 10);
            rec3.Location = new Point(75, 75);
            graficador.FillEllipse(Brushes.Black, rec3);
            graficador.DrawEllipse(Lapiz, rec3);
            Rectangle rec4 = new Rectangle();
            rec4.Size = new Size(10, 10);
            rec4.Location = new Point(75, 15);
            graficador.FillEllipse(Brushes.Black, rec4);
            graficador.DrawEllipse(Lapiz, rec4);
            Rectangle rec5 = new Rectangle();
            rec5.Size = new Size(10, 10);
            rec5.Location = new Point(15, 75);
            graficador.FillEllipse(Brushes.Black, rec5);
            graficador.DrawEllipse(Lapiz, rec5);
        }

        public void Retener_Soltar()
        {
            if(dado.Retenido)
            {
                if (dado.Valor == 1)
                {
                    graficador.Clear(Color.Red);
                    EscribirUno();
                }
                else if (dado.Valor == 2)
                {
                    graficador.Clear(Color.Red);
                    EscribirDos();
                }
                else if (dado.Valor == 3)
                {
                    graficador.Clear(Color.Red);
                    EscribirTres();
                }
                else if (dado.Valor == 4)
                {
                    graficador.Clear(Color.Red);
                    EscribirCuatro();
                }
                else if (dado.Valor == 5)
                {
                    graficador.Clear(Color.Red);
                    EscribirCinco();
                }
                else
                {
                    graficador.Clear(Color.Red);
                    EscribirSeis();
                }
            }
            else
            {
                MostrarValorDado();
            }
        }
    }
}
