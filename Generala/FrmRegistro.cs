﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Juego;

namespace Generala
{
    public partial class FrmRegistro : Form
    {
        public FrmRegistro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool ok = true;
            foreach (Control casilla in this.Controls)
            {
                if (casilla is MiCasilla)
                {
                    ok = ((MiCasilla)casilla).Validar() && ok;
                }
                if(comboBox1.Text == string.Empty)
                {
                    ok = false;
                }
            }
            if (!ok)
            {
                MessageBox.Show("Debe Completar todos los campos");
            }
            else
            {
                Jugador nuevo = new Jugador();
                nuevo.Informar += Nuevo_Informar;
                nuevo.Nombre = miCasilla1.Texto;
                nuevo.credencial.Usuario = miCasilla2.Texto;
                nuevo.credencial.Contraseña = miCasilla3.Texto;
                switch(comboBox1.Text)
                {
                    case "Bart":
                        nuevo.Imagen = @"Imagenes\Bart.jpg";
                        break;
                    case "Lisa":
                        nuevo.Imagen = @"Imagenes\Lisa.jpg";
                        break;
                    case "Marge":
                        nuevo.Imagen = @"Imagenes\Marge.jpg";
                        break;
                    case "Maggie":
                        nuevo.Imagen = @"Imagenes\Maggie.jpg";
                        break;
                    case "Tiburon":
                        nuevo.Imagen = @"Imagenes\Abuelo.jpg";
                        break;
                    case "Panda":
                        nuevo.Imagen = @"Imagenes\Burns.jpg";
                        break;
                    case "Homero":
                        nuevo.Imagen = @"Imagenes\Homero.jpg";
                        break;

                }
                nuevo.Insertar();
                Blanquear();
                this.Close();
            }
        }

        private void Nuevo_Informar(string mensaje)
        {
            MessageBox.Show(mensaje);
        }

        public void Blanquear()
        {
            miCasilla1.Blanquear();
            miCasilla2.Blanquear();
            miCasilla3.Blanquear();
        }

        private void FrmRegistro_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.Text == "Bart")
                {
                    //pictureBox1.BackColor = Color.White;
                    pictureBox1.Image = Image.FromFile(@"Imagenes\Bart.jpg");
                }
                else if(comboBox1.Text == "Lisa")
                {
                    pictureBox1.Image = Image.FromFile(@"Imagenes\Lisa.jpg");
                }
                else if (comboBox1.Text == "Marge")
                {
                    pictureBox1.Image = Image.FromFile(@"Imagenes\Marge.jpg");
                }
                else if (comboBox1.Text == "Burns")
                {
                    pictureBox1.Image = Image.FromFile(@"Imagenes\Burns.jpg");
                }
                else if (comboBox1.Text == "Maggie")
                {
                    pictureBox1.Image = Image.FromFile(@"Imagenes\Maggie.jpg");
                }
                else if (comboBox1.Text == "Abuelo")
                {
                    pictureBox1.Image = Image.FromFile(@"Imagenes\Abuelo.jpg");
                }
                else if (comboBox1.Text == "Homero")
                {
                    pictureBox1.Image = Image.FromFile(@"Imagenes\Homero.jpg");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void miCasilla1_Load(object sender, EventArgs e)
        {

        }

        private void miCasilla3_Load(object sender, EventArgs e)
        {

        }
    }
}
