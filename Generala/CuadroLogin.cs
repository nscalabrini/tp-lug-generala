﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Juego;

namespace Generala
{
    public partial class CuadroLogin : UserControl
    {
        public CuadroLogin()
        {
            InitializeComponent();
        }

        public Button Boton
        {
            get { return button1; }
            set { button1 = value; }
        }

        public Button Boton2
        {
            get { return button2; }
            set { button2 = value; }
        }

        public string Texto1
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }

        public string Texto2
        {
            get { return textBox2.Text; }
            set { textBox2.Text = value; }
        }

        public void CambiarColor(bool ok)
        {
            if (!ok)
            {
                textBox1.BackColor = Color.BlanchedAlmond;
                textBox2.BackColor = Color.BlanchedAlmond;
            }
            else
            {
                textBox1.BackColor = Color.White;
                textBox2.BackColor = Color.White;
            }
        }

        public bool Validar()
        {
            bool ok = (!string.IsNullOrWhiteSpace(textBox1.Text) || !string.IsNullOrWhiteSpace(textBox2.Text));
            CambiarColor(ok);
            return ok;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            label1.Visible = true;
            label2.Visible = true;
            textBox1.Visible = true;
            textBox2.Visible = true;
            button1.Visible = true;
        }

        private void CuadroLogin_Load(object sender, EventArgs e)
        {
            label1.Visible = false;
            label2.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            button1.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            button2.Visible = false;
        }

        public void EsconderControles()
        {
            textBox1.Visible = false;
            textBox2.Visible = false;
            label1.Visible = false;
            label2.Visible = false;
            button1.Visible = false;
        }

        public void MostrarDatos(Jugador j)
        {
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            button2.Visible = true;
            label3.Text = "Nombre: " + j.Nombre;
            label4.Text = "Usuario " + j.credencial.Usuario;
            label5.Text = "Partidas Ganadas: " + j.PartidasGanadas;
            pictureBox1.Visible = true;
            pictureBox1.Image = Image.FromFile(j.Imagen);
        }

        public void recargar()
        {
            Blanquear();
            pictureBox1.Visible = true;
            pictureBox1.Image = Image.FromFile(@"Imagenes\IconoAgregar.png");
            label1.Visible = false;
            label2.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            button1.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            button2.Visible = false;
        }

        public void Blanquear()
        {
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
        }
    }
}
