﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generala
{
    public partial class MiCasilla : UserControl
    {
        public MiCasilla()
        {
            InitializeComponent();
            
        }

        private bool requerido;

        public bool Requerido
        {
            get { return requerido; }
            set { requerido = value; }
        }

       
        public string Label
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }
        
        public string Texto
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }

        public void Blanquear()
        {
            textBox1.Text = string.Empty;
        }

        public void CambiarColor(bool ok)
        {
            if(!ok)
            {
                textBox1.BackColor = Color.BlanchedAlmond;
                label1.ForeColor = Color.Red;
                label3.ForeColor = Color.Red;
            }
            else
            {
                textBox1.BackColor = Color.White;
                label1.ForeColor = Color.White;
                label3.ForeColor = Color.White;
            }
        }

        public bool Validar()
        {
            bool ok = (!string.IsNullOrWhiteSpace(textBox1.Text) || !requerido);
            CambiarColor(ok);
            return ok;
        }


        private void MiCasilla_Load(object sender, EventArgs e)
        {
            if (requerido)
            {
                label3.Text = "(*)";
            }
            else
            {
                label3.Visible = false;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(requerido && textBox1.Text != string.Empty)
            {
                textBox1.BackColor = Color.White;
                label1.ForeColor = Color.White;
                label3.ForeColor = Color.White;
            }
            else
            {
                textBox1.BackColor = Color.BlanchedAlmond;
                label1.ForeColor = Color.Red;
                label3.ForeColor = Color.Red;
            }
        }
    }
}
