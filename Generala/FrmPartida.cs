﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Juego;

namespace Generala
{
    public partial class FrmPartida : Form
    {
        Bitacora bitacora = new Bitacora();
        public FrmPartida(List<Jugador> jugadores, Principal frmprincipal)
        {
            InitializeComponent();
            principal = frmprincipal;
            foreach(Jugador j in jugadores)
            {
                partida.AgregarJugador(j);
            }
            partida.IniciarPartida();
        }

        Principal principal = new Principal();
        Partida partida = new Partida();
        List<CU_UsuarioActivocs> Displays = new List<CU_UsuarioActivocs>();

        private void FrmPartida_Load(object sender, EventArgs e)
        {
            bitacora.Escribir(partida, "Inicio");
            partida.Finalizar += Partida_Finalizar;
            cU_UsuarioActivocs4.Visible = false;
            cU_UsuarioActivocs3.Visible = false;
            button2.Enabled = false;
            cU_UsuarioActivocs1.CambiarLabel(partida.Jugadores[0]);
            cU_UsuarioActivocs2.CambiarLabel(partida.Jugadores[1]);
            cU_UsuarioActivocs1.CambiarImagen(partida.Jugadores[0]);
            cU_UsuarioActivocs2.CambiarImagen(partida.Jugadores[1]);
            Displays.Add(cU_UsuarioActivocs1);
            Displays.Add(cU_UsuarioActivocs2);
            if (partida.Jugadores.Count == 4)
            {
                cU_UsuarioActivocs4.Visible = true;
                cU_UsuarioActivocs3.Visible = true;
                cU_UsuarioActivocs4.CambiarLabel(partida.Jugadores[3]);
                cU_UsuarioActivocs3.CambiarLabel(partida.Jugadores[2]);
                cU_UsuarioActivocs4.CambiarImagen(partida.Jugadores[3]);
                cU_UsuarioActivocs3.CambiarImagen(partida.Jugadores[2]);
                Displays.Add(cU_UsuarioActivocs3);
                Displays.Add(cU_UsuarioActivocs4);
            }
            else if(partida.Jugadores.Count == 3)
            {
                cU_UsuarioActivocs3.Visible = true;
                cU_UsuarioActivocs3.CambiarLabel(partida.Jugadores[2]);
                cU_UsuarioActivocs3.CambiarImagen(partida.Jugadores[2]);
                Displays.Add(cU_UsuarioActivocs3);
            }
            cU_UsuarioActivocs1.CambiarColor(1);
            progressBar1.Maximum = partida.Jugadores.Count * 10;
            progressBar1.Value = 1;
            LBLturno.Text = "Turno " + progressBar1.Value + "/" + progressBar1.Maximum;
            LBLturnosRestamtes.Text = "Tiradas restantes: " + partida.Activo.Turno.CantidadTiradas;
            foreach(Jugador j in partida.Jugadores)
            {
                j.Informar -= principal.J_Informar;
                j.Informar += J_Informar;
            }
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = partida.Activo.MisCombinatorias;
        }

        private void Partida_Finalizar(string mensaje)
        {
            
            if (mensaje.Equals("EMPATE"))
            {
                MessageBox.Show("Ha habido un empate");
                this.Close();
            }
            else
            {
                MessageBox.Show("El ganador es: " + partida.Ganador.credencial.Usuario);
                this.Close();
            }
        }

        private void J_Informar(string mensaje)
        {
            MessageBox.Show(mensaje);
        }

        private void FrmPartida_FormClosed(object sender, FormClosedEventArgs e)
        {
            bitacora.Escribir(partida, "Fin");
            foreach (Jugador j in partida.Jugadores)
            {
                j.Informar -= J_Informar;
            }
            principal.Show();
        }

        private void FrmPartida_Shown(object sender, EventArgs e)
        {
            cU_DADO1.EscribirPunto();
            cU_DADO2.EscribirPunto();
            cU_DADO3.EscribirPunto();
            cU_DADO4.EscribirPunto();
            cU_DADO5.EscribirPunto();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            partida.Activo.tirar();
            EstablecerValoresDados(partida.Activo.MisDados);
            cU_DADO1.MostrarValorDado();
            cU_DADO2.MostrarValorDado();
            cU_DADO3.MostrarValorDado();
            cU_DADO4.MostrarValorDado();
            cU_DADO5.MostrarValorDado();
            LBLturnosRestamtes.Text = "Tiradas restantes: " + partida.Activo.Turno.CantidadTiradas;
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = partida.EvaluarCombinatoria();
            button2.Enabled = true;
        }

        void EstablecerValoresDados(Dado[] dados)
        {
            cU_DADO1.dado = dados[0];
            cU_DADO2.dado = dados[1];
            cU_DADO3.dado = dados[2];
            cU_DADO4.dado = dados[3];
            cU_DADO5.dado = dados[4];
        }

        void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = partida.Activo.MisCombinatorias;
            dataGridView2.DataSource = null;
            LBLturnosRestamtes.Text = "Tiradas restantes: " + partida.Activo.Turno.CantidadTiradas;
            progressBar1.Value++;
            LBLturno.Text = "Turno " + progressBar1.Value + "/" + progressBar1.Maximum;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(dataGridView2.SelectedRows[0].DataBoundItem != null)
            {
                partida.Activo.ConsumirCombinatoria((CombinatoriaPuntaje)dataGridView2.SelectedRows[0].DataBoundItem);
                bitacora.Escribir(partida, partida.Activo, (CombinatoriaPuntaje)dataGridView2.SelectedRows[0].DataBoundItem);
                if(!partida.CambiarJugadorActivo())
                {
                    label1.Text = "Puntaje: " + partida.Activo.Puntaje;
                    foreach (CU_UsuarioActivocs disp in Displays)
                    {
                        if (disp.Texto.Equals(partida.Activo.credencial.Usuario))
                        {
                            if ((Displays.IndexOf(disp) - 1) > -1)
                            {
                                Displays[Displays.IndexOf(disp) - 1].CambiarColor(0);
                                disp.CambiarColor(1);
                            }
                            else
                            {
                                Displays[Displays.Count - 1].CambiarColor(0);
                                disp.CambiarColor(1);
                            }
                        }
                    }
                    Enlazar();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una combinatoria antes de finalizar el turno");
            }
        }

        private void cU_DADO1_Click(object sender, EventArgs e)
        {
            if(partida.Activo.Turno.CantidadTiradas != 3)
            {
                partida.Activo.Retener_Soltar(0);
                cU_DADO1.Retener_Soltar();
            }
            else
            {
                MessageBox.Show("Debe Tirar al menos una vez");
            }
        }
        
        private void cU_DADO2_Click(object sender, EventArgs e)
        {
            if (partida.Activo.Turno.CantidadTiradas != 3)
            {
                partida.Activo.Retener_Soltar(1);
                cU_DADO2.Retener_Soltar();
            }
            else
            {
                MessageBox.Show("Debe Tirar al menos una vez");
            }
        }
    
        private void cU_DADO3_Click(object sender, EventArgs e)
        {
            if (partida.Activo.Turno.CantidadTiradas != 3)
            {
                partida.Activo.Retener_Soltar(2);
                cU_DADO3.Retener_Soltar();
            }
            else
            {
                MessageBox.Show("Debe Tirar al menos una vez");
            }
        }
    
        private void cU_DADO4_Click(object sender, EventArgs e)
        {
            if (partida.Activo.Turno.CantidadTiradas != 3)
            {
                partida.Activo.Retener_Soltar(3);
                cU_DADO4.Retener_Soltar();
            }
            else
            {
                MessageBox.Show("Debe Tirar al menos una vez");
            }
        }
     
        private void cU_DADO5_Click(object sender, EventArgs e)
        {
            if (partida.Activo.Turno.CantidadTiradas != 3)
            {
                partida.Activo.Retener_Soltar(4);
                cU_DADO5.Retener_Soltar();
            }
            else
            {
                MessageBox.Show("Debe Tirar al menos una vez");
            }
        }
    
    }
}
