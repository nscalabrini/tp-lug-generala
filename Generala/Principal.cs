﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Juego;

namespace Generala
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        Bitacora bitacora = new Bitacora();
        private void Form1_Load(object sender, EventArgs e)
        {
            cuadroLogin1.Boton.Click += Boton_Click;
            cuadroLogin2.Boton.Click += Boton_Click1;
            cuadroLogin3.Boton.Click += Boton_Click2;
            cuadroLogin4.Boton.Click += Boton_Click3;
            cuadroLogin1.Boton2.Click += Boton2_Click;
            cuadroLogin2.Boton2.Click += Boton2_Click1;
            cuadroLogin3.Boton2.Click += Boton2_Click2;
            cuadroLogin4.Boton2.Click += Boton2_Click3;
        }

        private void Boton2_Click3(object sender, EventArgs e)
        {
            try
            {
                Jugador borrado = new Jugador();
                foreach (Jugador j in Jugadores)
                {
                    if (j.credencial.Usuario.Equals(cuadroLogin4.Texto1))
                    {
                        borrado = j;
                        cuadroLogin4.Blanquear();
                    }
                }
                bitacora.Escribir(borrado, "Cierre de Sesion");
                Jugadores.Remove(borrado);
                cuadroLogin4.recargar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Boton2_Click2(object sender, EventArgs e)
        {
            try
            {
                Jugador borrado = new Jugador();
                foreach (Jugador j in Jugadores)
                {
                    if (j.credencial.Usuario.Equals(cuadroLogin3.Texto1))
                    {
                        borrado = j;
                        cuadroLogin3.Blanquear();
                    }
                }
                bitacora.Escribir(borrado, "Cierre de Sesion");
                Jugadores.Remove(borrado);
                cuadroLogin3.recargar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Boton2_Click1(object sender, EventArgs e)
        {
            try
            {
                Jugador borrado = new Jugador();
                foreach (Jugador j in Jugadores)
                {
                    if (j.credencial.Usuario.Equals(cuadroLogin2.Texto1))
                    {
                        borrado = j;
                        cuadroLogin2.Blanquear();
                    }
                }
                bitacora.Escribir(borrado, "Cierre de Sesion");
                Jugadores.Remove(borrado);
                cuadroLogin2.recargar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Boton2_Click(object sender, EventArgs e)
        {
            try
            {
                Jugador borrado = new Jugador();
                foreach (Jugador j in Jugadores)
                {
                    if (j.credencial.Usuario.Equals(cuadroLogin1.Texto1))
                    {
                        borrado = j;
                        cuadroLogin1.Blanquear();
                    }
                }
                bitacora.Escribir(borrado, "Cierre de Sesion");
                Jugadores.Remove(borrado);
                cuadroLogin1.recargar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Boton_Click3(object sender, EventArgs e)
        {
            try
            {
                if (cuadroLogin4.Validar())
                {
                    Jugador j = new Jugador();
                    j.Informar += J_Informar;
                    j.credencial.Usuario = cuadroLogin4.Texto1;
                    j.credencial.Contraseña = cuadroLogin4.Texto2;
                    if (j.IniciarSesion())
                    {
                        if (!VerificarLogueados(j))
                        {
                            Jugadores.Add(j);
                            cuadroLogin4.EsconderControles();
                            cuadroLogin4.MostrarDatos(j);
                            bitacora.Escribir(j, "Inicio de Sesion");
                        }
                        else
                        {
                            MessageBox.Show("Usuario ya logueado.");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe completar todos los campos");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Boton_Click2(object sender, EventArgs e)
        {
            try
            {
                if (cuadroLogin3.Validar())
                {
                    Jugador j = new Jugador();
                    j.Informar += J_Informar;
                    j.credencial.Usuario = cuadroLogin3.Texto1;
                    j.credencial.Contraseña = cuadroLogin3.Texto2;
                    if (j.IniciarSesion())
                    {
                        if (!VerificarLogueados(j))
                        {
                            Jugadores.Add(j);
                            cuadroLogin3.EsconderControles();
                            cuadroLogin3.MostrarDatos(j);
                            bitacora.Escribir(j, "Inicio de Sesion");
                        }
                        else
                        {
                            MessageBox.Show("Usuario ya logueado.");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe completar todos los campos");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Boton_Click1(object sender, EventArgs e)
        {
            try
            {
                if (cuadroLogin2.Validar())
                {
                    Jugador j = new Jugador();
                    j.Informar += J_Informar;
                    j.credencial.Usuario = cuadroLogin2.Texto1;
                    j.credencial.Contraseña = cuadroLogin2.Texto2;
                    if (j.IniciarSesion())
                    {
                        if (!VerificarLogueados(j))
                        {
                            Jugadores.Add(j);
                            cuadroLogin2.EsconderControles();
                            cuadroLogin2.MostrarDatos(j);
                            bitacora.Escribir(j, "Inicio de Sesion");
                        }
                        else
                        {
                            MessageBox.Show("Usuario ya logueado.");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe completar todos los campos");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Boton_Click(object sender, EventArgs e)
        {
            try
            {
                if (cuadroLogin1.Validar())
                {
                    Jugador j = new Jugador();
                    j.Informar += J_Informar;
                    j.credencial.Usuario = cuadroLogin1.Texto1;
                    j.credencial.Contraseña = cuadroLogin1.Texto2;
                    if (j.IniciarSesion())
                    {
                        if (!VerificarLogueados(j))
                        {
                            Jugadores.Add(j);
                            cuadroLogin1.EsconderControles();
                            cuadroLogin1.MostrarDatos(j);
                            bitacora.Escribir(j, "Inicio de Sesion");
                        }
                        else
                        {
                            MessageBox.Show("Usuario ya logueado.");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe completar todos los campos");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void J_Informar(string mensaje)
        {
            MessageBox.Show(mensaje);
        }

        List<Jugador> Jugadores = new List<Jugador>();

        private void button2_Click(object sender, EventArgs e)
        {
            FrmRegistro Nuevo = new FrmRegistro();
            Nuevo.ShowDialog(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(Jugadores.Count >= 2)
            {
                FrmPartida Nuevo = new FrmPartida(Jugadores, this);
                this.Hide();
                Nuevo.ShowDialog(this);
            //    Jugadores.Clear();
            //    cuadroLogin1.recargar();
            //    cuadroLogin2.recargar();
            //    cuadroLogin3.recargar();
            //    cuadroLogin4.recargar();
            }
            else
            {
                MessageBox.Show("Deben haber al menos 2 jugadores para iniciar la partida.");
            }
            
        }

        bool VerificarLogueados(Jugador j)
        {
            bool ok = false;
            foreach(Jugador jug in Jugadores)
            {
                if(j.credencial.Usuario.Equals(jug.credencial.Usuario))
                {
                    ok = true;
                }
            }
            return ok;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmEstadisticas Nuevo = new FrmEstadisticas();
            Nuevo.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmBitacora Nuevo = new FrmBitacora();
            Nuevo.ShowDialog();
        }

        private void Principal_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach(Jugador j in Jugadores)
            {
                bitacora.Escribir(j, "Cierre de Sesion");
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
