﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Juego;
using System.Data;
using System.IO;

namespace Generala
{
    class Bitacora
    {
        static string RutaLogin = @"C:\Users\norberto.scalabrini\Desktop\TMP\UAI\LUG Local\LUG2019\Generala\TP Generala\BitacoraLogin.xml";
        static string EsquemaLogin = @"C:\Users\norberto.scalabrini\Desktop\TMP\UAI\LUG Local\LUG2019\Generala\TP Generala\esquemaLogin.xml";
        static string RutaMovimientos = @"C:\Users\norberto.scalabrini\Desktop\TMP\UAI\LUG Local\LUG2019\Generala\TP Generala\BitacoraMovimientos.xml";
        static string EsquemaMovimientos = @"C:\Users\norberto.scalabrini\Desktop\TMP\UAI\LUG Local\LUG2019\Generala\TP Generala\esquemaMovimientos.xml";
        static string RutaPartida = @"C:\Users\norberto.scalabrini\Desktop\TMP\UAI\LUG Local\LUG2019\Generala\TP Generala\BitacoraPartida.xml";
        static string EsquemaPartida = @"C:\Users\norberto.scalabrini\Desktop\TMP\UAI\LUG Local\LUG2019\Generala\TP Generala\esquemaPartida.xml";

        XmlDocument xmldoc = new XmlDocument();
        XmlNode nodoactualLogin;
        XmlNode nodoactualMovimiento;
        XmlNode nodoactualPartida;

        public DataSet Leer()
        {
            DataSet ds = new DataSet();
            if (!File.Exists(RutaLogin))
            {
                DataTable tabla = new DataTable("BITACORALOGIN");
                tabla.Columns.Add("Nombre");
                tabla.Columns.Add("Usuario");
                tabla.Columns.Add("Tipo");
                tabla.Columns.Add("Fecha");
                ds.Tables.Add(tabla);
                ds.WriteXml(RutaLogin);
                ds.WriteXmlSchema(EsquemaLogin);
            }
            else
            {
                ds.ReadXml(RutaLogin);
                ds.ReadXmlSchema(EsquemaLogin);
            }
            return ds;
        }

        public void Escribir(Jugador j, string tipo)
        {
            DataSet ds = Leer();
            DataRow registro = ds.Tables[0].NewRow();
            registro["Nombre"] = j.Nombre;
            registro["Usuario"] = j.credencial.Usuario;
            registro["Tipo"] = tipo;
            registro["Fecha"] = DateTime.Now;
            ds.Tables[0].Rows.Add(registro);
            ds.WriteXml(RutaLogin);
        }

        public DataSet LeerMovimientos()
        {
            DataSet ds = new DataSet();
            if (!File.Exists(RutaMovimientos))
            {
                DataTable tabla = new DataTable("BITACORAMOVIMIENTOS");
                tabla.Columns.Add("Partida");
                tabla.Columns.Add("Usuario");
                tabla.Columns.Add("Combinatoria");
                tabla.Columns.Add("Puntos");
                tabla.Columns.Add("PuntajeJugador");
                tabla.Columns.Add("Fecha");
                ds.Tables.Add(tabla);
                ds.WriteXml(RutaMovimientos);
                ds.WriteXmlSchema(EsquemaMovimientos);
            }
            else
            {
                ds.ReadXml(RutaMovimientos);
                ds.ReadXmlSchema(EsquemaMovimientos);
            }
            return ds;
        }
        public void Escribir(Partida p, Jugador j, CombinatoriaPuntaje cp)
        {
            DataSet ds = LeerMovimientos();
            DataRow registro = ds.Tables[0].NewRow();
            registro["Partida"] = p.ID;
            registro["Usuario"] = j.credencial.Usuario;
            registro["Combinatoria"] = cp.ToString();
            registro["Puntos"] = cp.Puntaje;
            registro["PuntajeJugador"] = j.Puntaje;
            registro["Fecha"] = DateTime.Now;
            ds.Tables[0].Rows.Add(registro);
            ds.WriteXml(RutaMovimientos);
        }

        public DataSet LeerPartida()
        {
            DataSet ds = new DataSet();
            if (!File.Exists(RutaPartida))
            {
                DataTable tabla = new DataTable("BITACORAPARTIDA");
                tabla.Columns.Add("Partida");
                tabla.Columns.Add("Tipo");
                tabla.Columns.Add("Fecha");
                ds.Tables.Add(tabla);
                ds.WriteXml(RutaPartida);
                ds.WriteXmlSchema(EsquemaPartida);
            }
            else
            {
                ds.ReadXml(RutaPartida);
                ds.ReadXmlSchema(EsquemaPartida);
            }
            return ds;
        }
        public void Escribir(Partida p, string tipo)
        {
            DataSet ds = LeerPartida();
            DataRow registro = ds.Tables[0].NewRow();
            registro["Partida"] = p.ID;
            registro["Tipo"] = tipo;
            registro["Fecha"] = DateTime.Now;
            ds.Tables[0].Rows.Add(registro);
            ds.WriteXml(RutaPartida);
        }

        public XmlNode LoadLogin()
        {
            xmldoc.Load(RutaLogin);
            nodoactualLogin = xmldoc.DocumentElement;
            return nodoactualLogin;
        }

        public XmlNode LoadMovimiento()
        {
            xmldoc.Load(RutaMovimientos);
            nodoactualMovimiento = xmldoc.DocumentElement;
            return nodoactualMovimiento;
        }

        public XmlNode LoadPartida()
        {
            xmldoc.Load(RutaPartida);
            nodoactualPartida = xmldoc.DocumentElement;
            return nodoactualPartida;
        }

        public XmlNode MostrarSiguiente(XmlNode actual)
        {
            try
            {
                if(actual.NextSibling != null)
                {
                    actual = actual.NextSibling;
                }
                return actual;
            }
            catch(Exception exep)
            {
                return null;
            }
        }

        public XmlNode MostrarAnterior(XmlNode actual)
        {
            try
            {
                if(actual.PreviousSibling != null)
                {
                    actual = actual.PreviousSibling;
                }
                return actual;
            }
            catch (Exception exep)
            {
                return null;
            }
        }

        public XmlNode MostrarPrimero(XmlNode actual)
        {
            try
            {
                if(actual.HasChildNodes)
                {
                    actual = actual.FirstChild;
                }
                return actual;
            }
            catch (Exception exep)
            {
                return null;
            }
        }

        public XmlNode MostrarUltimo(XmlNode actual)
        {
            try
            {
                if(actual.HasChildNodes)
                {
                    actual = actual.LastChild;
                    
                }
                return actual;
            }
            catch (Exception exep)
            {
                return null;
            }
        }

        public XmlNode MostrarPadre(XmlNode actual)
        {
            try
            {
                if (actual.ParentNode != null)
                {
                    actual = actual.ParentNode;
                }
                return actual;
            }
            catch (Exception exep)
            {
                return null;
            }
        }
    }
}
