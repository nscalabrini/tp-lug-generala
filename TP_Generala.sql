USE [master]
GO
/****** Object:  Database [Generala]    Script Date: 18/11/2020 20:04:34 ******/
CREATE DATABASE [Generala]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Generala', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Generala.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Generala_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Generala_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Generala] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Generala].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Generala] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Generala] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Generala] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Generala] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Generala] SET ARITHABORT OFF 
GO
ALTER DATABASE [Generala] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Generala] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Generala] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Generala] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Generala] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Generala] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Generala] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Generala] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Generala] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Generala] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Generala] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Generala] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Generala] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Generala] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Generala] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Generala] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Generala] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Generala] SET RECOVERY FULL 
GO
ALTER DATABASE [Generala] SET  MULTI_USER 
GO
ALTER DATABASE [Generala] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Generala] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Generala] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Generala] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Generala] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Generala', N'ON'
GO
ALTER DATABASE [Generala] SET QUERY_STORE = OFF
GO
USE [Generala]
GO
/****** Object:  Table [dbo].[Jugador]    Script Date: 18/11/2020 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jugador](
	[id_jugador] [int] NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Usuario] [nvarchar](50) NOT NULL,
	[Contraseña] [nvarchar](50) NOT NULL,
	[PartidasGanadas] [int] NOT NULL,
	[PartidasEmpatadas] [int] NOT NULL,
	[PartidasPerdidas] [int] NOT NULL,
	[Imagen] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Jugador] PRIMARY KEY CLUSTERED 
(
	[id_jugador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Partida]    Script Date: 18/11/2020 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Partida](
	[id_Partida] [int] NOT NULL,
	[id_Ganador] [int] NOT NULL,
	[Fecha] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_Partida] PRIMARY KEY CLUSTERED 
(
	[id_Partida] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Partida_Jugador]    Script Date: 18/11/2020 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Partida_Jugador](
	[id_Partida] [int] NOT NULL,
	[id_jugador] [int] NOT NULL,
	[puntaje] [int] NOT NULL,
 CONSTRAINT [PK_Partida_Jugador] PRIMARY KEY CLUSTERED 
(
	[id_Partida] ASC,
	[id_jugador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Jugador] ADD  CONSTRAINT [DF_Jugador_PartidasEmaptadas]  DEFAULT ((0)) FOR [PartidasEmpatadas]
GO
ALTER TABLE [dbo].[Partida]  WITH CHECK ADD  CONSTRAINT [FK_Partida_Jugador] FOREIGN KEY([id_Ganador])
REFERENCES [dbo].[Jugador] ([id_jugador])
GO
ALTER TABLE [dbo].[Partida] CHECK CONSTRAINT [FK_Partida_Jugador]
GO
ALTER TABLE [dbo].[Partida_Jugador]  WITH CHECK ADD  CONSTRAINT [FK_Partida_Jugador_Jugador] FOREIGN KEY([id_jugador])
REFERENCES [dbo].[Jugador] ([id_jugador])
GO
ALTER TABLE [dbo].[Partida_Jugador] CHECK CONSTRAINT [FK_Partida_Jugador_Jugador]
GO
ALTER TABLE [dbo].[Partida_Jugador]  WITH CHECK ADD  CONSTRAINT [FK_Partida_Jugador_Partida] FOREIGN KEY([id_Partida])
REFERENCES [dbo].[Partida] ([id_Partida])
GO
ALTER TABLE [dbo].[Partida_Jugador] CHECK CONSTRAINT [FK_Partida_Jugador_Partida]
GO
/****** Object:  StoredProcedure [dbo].[ASIGNAR_ID_PARTIDA]    Script Date: 18/11/2020 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ASIGNAR_ID_PARTIDA]
as
begin
select(ISNULL(MAX(id_partida),0)) from Partida
end
GO
/****** Object:  StoredProcedure [dbo].[EDITAR_JUGADOR]    Script Date: 18/11/2020 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[EDITAR_JUGADOR]
@id int, @PartidasGanadas int, @PartidasEmpatadas int, @PartidasPerdidas int
as
begin
update Jugador set PartidasGanadas = @PartidasGanadas, PartidasEmpatadas = @PartidasEmpatadas, PartidasPerdidas = @PartidasPerdidas where @id = id_jugador
end
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_JUGADOR]    Script Date: 18/11/2020 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[INSERTAR_JUGADOR]
@nombre nvarchar(50), @usuario nvarchar(50), @contra nvarchar(50), @Imagen nvarchar(100)
as
begin
declare @id int
set @id = (select(ISNULL(max(id_jugador),0)) from Jugador) + 1
insert into Jugador values (@id,@nombre,@usuario,@contra,0,0,0,@Imagen)
end
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_PARTIDA]    Script Date: 18/11/2020 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[INSERTAR_PARTIDA]
@id int, @id_Ganador int, @fecha numeric(18,0)
as
begin
if @id_Ganador = 0 insert into Partida values (@id,null,@fecha)
else
insert into Partida values (@id,@id_Ganador,@fecha)
end
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_PARTIDA_JUGADOR]    Script Date: 18/11/2020 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[INSERTAR_PARTIDA_JUGADOR]
@id int, @id_jugador int, @puntaje int
as
begin
insert into Partida_Jugador values (@id,@id_jugador,@puntaje)
end
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_JUGADOR]    Script Date: 18/11/2020 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[LISTAR_JUGADOR]
as
begin
select * from Jugador
end
GO
/****** Object:  StoredProcedure [dbo].[VERIFICAR_JUGADOR]    Script Date: 18/11/2020 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[VERIFICAR_JUGADOR]
@usuario nvarchar(50)
as
begin
select count(Usuario) from Jugador where @usuario = Usuario
end
GO
USE [master]
GO
ALTER DATABASE [Generala] SET  READ_WRITE 
GO
